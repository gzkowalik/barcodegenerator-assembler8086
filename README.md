This is a barcode generator, which can be compiled with emu8086.
How to use:
Launch the program from command prompt with string, which you want to change to barcode, as an argument.
You can write any string with length 1-24 (because of application's screen resolution).
Once you will push Enter, the program generates barcode which you can check in barcode reader app.
Put ESC to quit to command prompt.